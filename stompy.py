# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 09:12:25 2019

@author: bertr
"""
import random
import matplotlib.pyplot as plt

cardnames = ["F forest", "D dryad militant", "E experiment one", "B barkhide troll", "A avatar of the resolute", "2 2-drop", "3 3-drop", "P pump spell", "S spell"]
cardnamesShort = ["forest", "dryad", "experiment", "troll", "avatar", "2-drop", "3-drop", "AoH", "spell"]
CMC = [0,1,1,2,2,2,3,1,1]
power = [0,2,1,2,3,3,5,0,0]
toughness = [0,1,1,2,2,3,4,0,0]
creature = [False, True, True, True, True, True, True, False, False]


class Game:
    def __init__(self):
        self.deck = []                          #True Cards
        self.setupDeck([20,4,8,4,4,4,6,4,6])
        self.randomizeDeck()
        
        self.hand = []                          #index
        self.board = []                         #index
        self.summoningSick = []                 #index
        self.cardDrawnThisTurn = []             #index
        
        self.topOfDeck = 0                      #index
        
        self.lands = 0
        self.mana = 0
        self.turn = 0
        self.damage = 0
        self.boardPowers = []
        self.usePump = True
        
        self.verbose = False

    def randomizeDeck(self):
        random.shuffle(self.deck)
        for i in range(len(self.deck)):
            self.deck[i].key = i

    def setupDeck(self, composition):
        for i in range(len(composition)):
            self.addCards(cardnames[i], composition[i])
        
    def addCards(self, name, number):
        index = cardnames.index(name)
        for i in range(number):
            self.deck.append(Card(index, len(self.deck)))
            
    def start(self):
        self.drawHand()
        #mulligan
        for i in range(4):
            self.playTurn(i)
        
        
    def drawHand(self):
        self.hand = list(range(7))
        self.topOfDeck = 7
        
    def playTurn(self, i):
        self.turn = i+1
        if i is not 0:
            self.cardDrawnThisTurn.append(self.topOfDeck)
            self.hand.append(self.topOfDeck)
            self.topOfDeck += 1
    
        if self.verbose:    
            print(self.handComposition())
    
        self.playLand()
        self.mana = self.lands
        
        self.playCreatures()
        self.attack()
        
        if self.verbose:
            print(self.gameState())
        
        #EOT
        self.boardPowers.append(self.powerOnTheBoard())
        self.cardDrawnThisTurn = []
        self.summoningSick = []
        
    def playLand(self):
        for i in self.hand:
            if self.deck[i].index is 0: #forest
                self.hand.remove(i)
                self.lands += 1
                break
            
    def playCreatures(self):
        creatures = [i for i in self.hand if self.deck[i].isCreature]
        
        choices = {}
        self.subset(creatures, self.mana, 0, len(creatures) - 1, [], choices)
        print(f"Choices : {choices}")
            
        
        for i in self.hand:
            card = self.deck[i]
            if card.isCreature and card.CMC <= self.mana:
                self.playCard(i)
                break
        
        
        
    def subset(self, creatures, maxMana, l, r, choice, choices):
        mana = sum([self.deck[i]].CMC for i in choice)
        if l > r:
            choices[mana].append(choice)
            return
        
        if mana + self.deck[creatures[l]].CMC < maxMana:
            self.subset(creatures, maxMana, l+1, r, choice + [l], choices)
            
        self.subset(creatures, maxMana, l+1, r, choice, choices)
            
    def playCard(self, i):
        card = self.deck[i]
        self.mana -= card.CMC
        self.hand.remove(i)
        self.board.append(i)
        self.summoningSick.append(i)
        
        if card.name is "troll":
            card.addMarker()
        elif card.name is "avatar":
            nbMarker = 0
            for j in self.board:
                creature = self.deck[j]
                if creature.marker > 0:
                    nbMarker += 1
            card.addMarker(nbMarker)
         
        # Evolve trigger
        for j in self.board:
            creature = self.deck[j]
            if creature.name is "experiment" and card.power > creature.power:
                creature.addMarker()
                if self.verbose:
                    print(f"evolve because of {card.name}. Its power is now {creature.power}")
                
            

    def handHasCreature(self):
        return any([self.deck[i].isCreature for i in self.hand])
        
    def biggestCreatureIndex(self):
        pass
    
    def attack(self):
        for i in list(set(self.board) - set(self.summoningSick)):
            self.damage += self.deck[i].power
            
    def powerOnTheBoard(self):
        return sum([self.deck[i].power for i in self.board])
    
    def __str__(self):
        return f"Game"
    
    def gameState(self):
        board = ",".join([cardnamesShort[self.deck[i].index] for i in self.board])
        if len(self.cardDrawnThisTurn) == 0:
            drawn = "nothing"
        elif len(self.cardDrawnThisTurn) == 1:
            drawn = f"a {cardnamesShort[self.deck[self.cardDrawnThisTurn[0]].index]}"
        else: 
            drawn = ",".join([self.deck[i].name for i in self.cardDrawnThisTurn])
        return (f"Turn {self.turn}, player has {self.lands} lands, "
                f"{len(self.board)} creatures ({board}) with power {self.powerOnTheBoard()}, "
                f"has drawn {drawn} and has dealt {self.damage} damage")
        
    def handComposition(self):
        s = f"Player has {len(self.hand)} cards:\n\t"
        s += "\n\t".join([f"{index + 1}\t{self.deck[self.hand[index]].longname}" for index in range(len(self.hand))])
        #s += "\n\t".join([f"{index + 1}\t{self.hand[index]}" for index in range(leself.hand)])
        return s
    
    def deckComposition(self, nbCardsShown = -1):
        if nbCardsShown < 0:
            end = len(self.deck)
        else:
            end = min(len(self.deck), self.topOfDeck + nbCardsShown)
        s = f"Deck has {len(self.deck)} cards:\n\t"
        s += "\n\t".join([f"{(self.deck[index].key + 1)}\t{self.deck[index].longname}" for index in range(self.topOfDeck, end)])
        return s
    
    
class Card:
    def __init__(self, index, key):
        self.index = index
        self.longname = cardnames[index]
        self.name = cardnamesShort[index]
        self.power = power[index]
        self.toughness = toughness[index]
        self.isCreature = creature[index]
        self.CMC = CMC[index]
        self.key = key
        self.marker = 0

        
    def addMarker(self, nbMarker = 1):
        self.marker += nbMarker
        self.power += nbMarker
        self.toughness += nbMarker
        
        
    def play(self, game):
        pass
    
#%%
        
def plot_damage(damage):
  plt.figure(figsize = (3,3))
  histogram = [0]*21
  for d in damage:
      histogram[d] += 1
  
  maxVal = max(histogram)
  step = maxVal//4
  yticks = list(range(0,3*step+1,step))
  yticks.append(maxVal)
  
  plt.grid(False)
  plt.xticks(range(0,21,5))
  plt.yticks(yticks)
  plt.bar(range(21), histogram, color="#777777")
  plt.xlabel(f"Average damage: {sum(damage)/len(damage)}")

#%% 
N = 1000
damage = []
for i in range(N):
    g = Game()
    g.start()
    damage.append(min(g.damage, 20))

print(f"Avg dmg : {sum(damage)/N}")   

plot_damage(damage)

#%%
        
random.seed(1)
g = Game()
g.verbose = True
g.start() 